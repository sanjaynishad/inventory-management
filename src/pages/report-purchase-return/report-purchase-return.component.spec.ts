import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportPurchaseReturnComponent } from './report-purchase-return.component';

describe('ReportPurchaseReturnComponent', () => {
  let component: ReportPurchaseReturnComponent;
  let fixture: ComponentFixture<ReportPurchaseReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportPurchaseReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportPurchaseReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
