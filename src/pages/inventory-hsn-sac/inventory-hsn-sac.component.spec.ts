import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryHsnSacComponent } from './inventory-hsn-sac.component';

describe('InventoryHsnSacComponent', () => {
  let component: InventoryHsnSacComponent;
  let fixture: ComponentFixture<InventoryHsnSacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryHsnSacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryHsnSacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
