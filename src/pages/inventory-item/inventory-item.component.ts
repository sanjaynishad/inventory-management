import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';

import { InventoryItem } from './../../app/models';

@Component({
  selector: 'app-inventory-item',
  templateUrl: './inventory-item.component.html',
  styleUrls: ['./inventory-item.component.scss']
})
export class InventoryItemComponent implements OnInit {
  item: InventoryItem = new InventoryItem();
  selectedItem: InventoryItem;
  newItem: boolean;
  displayDialog: boolean;


  items: InventoryItem[] = [];
  cols: any[];

  statuses: SelectItem[];
  units: SelectItem[];
  hsnSacList: SelectItem[];


  constructor() { }

  ngOnInit() {

    this.cols = [
      { field: 'code', header: 'Code' },
      { field: 'name', header: 'Name' },
      { field: 'hsnSac', header: 'HSN/SAC' },
      { field: 'sgst', header: 'SGST' },
      { field: 'cgst', header: 'CGST' },
      { field: 'igst', header: 'IGST' },
      { field: 'mrp', header: 'MRP' },
      { field: 'rate', header: 'Rate' },
      { field: 'status', header: 'Status' },
    ];

    this.statuses = [{
      label: 'Active',
      value: true,
    }, {
      label: 'Inactive',
      value: false,
    }];

    this.units = [{
      label: 'Pcs',
      value: 'Pcs'
    }, {
      label: 'Box',
      value: 'Box'
    }];

    this.hsnSacList = [{
      label: '123',
      value: 123,
    }, {
      label: '567',
      value: 567
    }]
  }

  showDialogToAdd() {
    this.newItem = true;
    this.item = new InventoryItem();
    this.displayDialog = true;
  }

  save() {
    const items = [...this.items];
    if (this.newItem) {
      items.push(this.item);
    } else {
      items[this.findSelectedItemIndex()] = this.item;
    }
    this.items = items;
    this.item = null;
    this.displayDialog = false;
  }

  delete() {
    this.items = this.items.filter(d => d.id != this.selectedItem.id);
    this.item = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.newItem = false;
    this.item = { ...event.data };
    this.displayDialog = true;
  }

  findSelectedItemIndex(): number {
    return this.items.indexOf(this.selectedItem);
  }

}
