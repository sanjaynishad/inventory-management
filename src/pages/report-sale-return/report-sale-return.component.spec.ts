import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSaleReturnComponent } from './report-sale-return.component';

describe('ReportSaleReturnComponent', () => {
  let component: ReportSaleReturnComponent;
  let fixture: ComponentFixture<ReportSaleReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSaleReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSaleReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
