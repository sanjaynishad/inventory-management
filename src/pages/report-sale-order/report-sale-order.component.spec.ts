import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSaleOrderComponent } from './report-sale-order.component';

describe('ReportSaleOrderComponent', () => {
  let component: ReportSaleOrderComponent;
  let fixture: ComponentFixture<ReportSaleOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSaleOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSaleOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
