import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryUnitComponent } from './inventory-unit.component';

describe('InventoryUnitComponent', () => {
  let component: InventoryUnitComponent;
  let fixture: ComponentFixture<InventoryUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
