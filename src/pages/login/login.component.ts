import { Component, OnInit, ViewChildren, AfterViewInit } from '@angular/core';
import { AuthService } from './../../services';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  userName = '';
  password = '';
  @ViewChildren('userNameEl') userNameEl;
  constructor(
    private authService: AuthService,
    private router: Router,
    private messageService: MessageService,
    private electron: ElectronService,
  ) {
    if (this.authService.currentUser) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.userNameEl.first.nativeElement.focus();
  }

  submit() {
    let msg;
    if (!this.userName) {
      msg = 'Please enter UserName or Email to login.';
    } else if (!this.password) {
      msg = 'Please enter password to login.';
    }

    if (msg) {
      this.messageService.add({ severity: 'error', summary: 'Login Failed', detail: msg });
      return false;
    }

    this.authService.currentUser = true;
    this.router.navigate(['/']);
  }

  close() {
    this.electron.remote.getCurrentWindow().close();
  }
}
