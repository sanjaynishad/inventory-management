import { Component, OnInit } from '@angular/core';
import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-title-bar',
  templateUrl: './title-bar.component.html',
  styleUrls: ['./title-bar.component.scss'],
})
export class TitleBarComponent implements OnInit {
  title = ' - Login';
  showRestoreBtn = false;
  remote;

  constructor(
    private electronService: ElectronService
  ) {
    this.remote = this.electronService.remote;
  }

  ngOnInit() {
  }

  minimize() {
    let win = this.remote.getCurrentWindow();
    win.minimize();
  }

  close() {
    let win = this.remote.getCurrentWindow();
    win.close();
  }

}
