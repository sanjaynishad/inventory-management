import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-theme-switch',
  templateUrl: './theme-switch.component.html',
  styleUrls: ['./theme-switch.component.scss']
})
export class ThemeSwitchComponent implements OnInit {
  themes = [{
    id: 'nova-dark',
    name: 'Nova Dark'
  }, {
    id: 'nova-light',
    name: 'Nova Light'
  }, {
    id: 'nova-colored',
    name: 'Nova Colored'
  }, {
    id: 'luna-amber',
    name: 'Luna Amber'
  }, {
    id: 'luna-blue',
    name: 'Luna Blue'
  }, {
    id: 'luna-green',
    name: 'Luna Green'
  }, {
    id: 'luna-pink',
    name: 'Luna Pink'
  }, {
    id: 'rhea',
    name: 'Rhea'
  }];

  selectedTheme;
  constructor() {
    let id = localStorage.getItem('app-theme') || 'nova-dark';
    this.selectedTheme = this.themes.filter(d => d.id == id)[0];
    this.switchTheme();
  }

  ngOnInit() {
  }

  switchTheme() {
    let link: any = document.getElementById('primeng-theme-link');
    link.href = `assets/libs/primeng/themes/${this.selectedTheme.id}/theme.css`;
    localStorage.setItem('app-theme', this.selectedTheme.id);
  }

}
