import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/primeng';

import {
  DataTableModule,
} from 'primeng/primeng'

const PRIMENG_MODULES = [
  InputTextModule,
  ButtonModule,
  TableModule,
  DialogModule,
  MultiSelectModule,
  DataTableModule,
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES
  ]
})
export class ThemeModule { }
