import { Component } from '@angular/core';
import { MenuItem } from 'primeng/primeng';

import { AuthService } from './../services';
import { MENUS, setThemeOnAppLoad } from './menus';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    items: MenuItem[];

    constructor(
        private authService: AuthService
    ) { 
        setThemeOnAppLoad();
    }

    ngOnInit() {
        this.items = MENUS;
    }

    logout() {
        this.authService.logout();
    }
}
