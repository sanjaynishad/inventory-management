import { MenuItem } from 'primeng/primeng';

const themesMenuItems = [{
    id: 'nova-colored',
    label: 'Nova Colored',
    styleClass: 'theme-menu-item',
    command: switchTheme
}, {
    id: 'nova-dark',
    label: 'Nova Dark',
    command: switchTheme,
    styleClass: 'theme-menu-item',
    icon: 'fa fa-check'
}, {
    id: 'nova-light',
    label: 'Nova Light',
    styleClass: 'theme-menu-item',
    command: switchTheme
}, {
    separator: true
}, {
    id: 'luna-amber',
    label: 'Luna Amber',
    styleClass: 'theme-menu-item',
    command: switchTheme
}, {
    id: 'luna-blue',
    label: 'Luna Blue',
    styleClass: 'theme-menu-item',
    command: switchTheme
}, {
    id: 'luna-green',
    label: 'Luna Green',
    styleClass: 'theme-menu-item',
    command: switchTheme
}, {
    id: 'luna-pink',
    label: 'Luna Pink',
    styleClass: 'theme-menu-item',
    command: switchTheme
}, {
    separator: true
}, {
    id: 'rhea',
    label: 'Rhea',
    styleClass: 'theme-menu-item',
    command: switchTheme
}];

export function setThemeOnAppLoad() {
    let id = localStorage.getItem('app-theme') || 'nova-colored';
    switchTheme({ item: { id: id } });
}

function switchTheme(e) {
    let id = e.item.id || 'nova-colored';
    let link: any = document.getElementById('primeng-theme-link');
    link.href = `assets/libs/primeng/themes/${id}/theme.css`;

    delete themesMenuItems.filter(item => item.icon)[0].icon;
    themesMenuItems.filter(item => item.id == id)[0].icon = 'fa fa-check';

    document.getElementsByTagName('body')[0].className = id;
    localStorage.setItem('app-theme', id);
}

export const MENUS: MenuItem[] = [
    // {
    //     label: 'File',
    //     // icon: 'pi pi-fw pi-file',
    //     items: [{
    //         label: 'New',
    //         icon: 'pi pi-fw pi-plus',
    //         items: [
    //             { label: 'Project' },
    //             { label: 'Other' },
    //         ]
    //     },
    //     { label: 'Open' },
    //     { separator: true },
    //     { label: 'Quit' }
    //     ]
    // },
    {
        label: 'Master',
        // icon: 'pi pi-fw pi-pencil',
        items: [
            {
                label: 'Inventory',
                // icon: 'pi pi-fw pi-trash',
                items: [{
                    label: 'Item',
                    routerLink: 'inventory-item',
                }, {
                    label: 'Unit',
                    routerLink: 'inventory-unit',
                }, {
                    label: 'HSN/SAC',
                    routerLink: 'inventory-hsn-sac',
                }, {
                    label: 'Store',
                    routerLink: 'inventory-store',
                }],
            },
            // { label: 'Refresh', icon: 'pi pi-fw pi-refresh' }
        ]
    }, {
        label: 'Transactions',
        items: [{
            label: 'Sale',
        }, {
            label: 'Sale Order'
        }, {
            label: 'Sale Return',
        }, {
            separator: true
        }, {
            label: 'Purchase',
        }, {
            label: 'Purchase Order'
        }, {
            label: 'Purchase Return'
        }]
    },
    {
        label: 'Reports',
        items: [{
            label: 'Stocks'
        }, {
            label: 'Price List'
        }, {
            label: 'Sale',
            items: [{
                label: 'Sale'
            }, {
                label: 'Sale Order'
            }, {
                label: 'Sale Return'
            }]
        }, {
            label: 'Purchase',
            items: [{
                label: 'Purchase',
            }, {
                label: 'Purchase Order'
            }, {
                label: 'Purchase Return'
            }]
        }],
    }, {
        label: 'Tools',
        // icon: 'pi pi-fw pi-cog',
        items: [
            {
                label: 'Setting',
                icon: 'fa fa-gear',
            },
            {
                label: 'Switch Themes',
                icon: 'fa fa-retweet',
                items: themesMenuItems,
            }, {
                separator: true
            }, {
                label: 'Import Data',
                icon: 'fa fa-upload',
            },
            {
                label: 'Export Data',
                icon: 'fa fa-download',
            }
        ]
    },
    {
        label: 'Help',
        // icon: 'pi pi-fw pi-question',
        items: [
            {
                label: 'Contents',
                icon: 'fa fa-file-text',
            }, {
                label: 'Website',
                icon: 'fa fa-globe',
                items: [
                    {
                        label: 'Your Company Portal',
                    },
                    {
                        label: 'Our Website',
                    },
                ]
            }, {
                label: 'Support Tickets',
                icon: 'fa fa-ticket',
            }, {
                separator: true
            }, {
                label: 'About Us',
                icon: 'fa fa-users',
            }
        ]
    },
];
