export enum GstCodeType {
    GOODS = 1,
    SERVICES = 2
}

export class InventoryItem {
    id: string;
    code: string;
    name: string;
    description: string;
    unit: string;
    keywords: string;
    hsnSac: number;
    sgst: number;
    cgst: number;
    igst: number;
    cess: number;
    mrp: number;
    rate: number;
    discount: string;
    status: boolean;
}

export interface InventoryUnit {
    id: string;
    name: string;
}

export interface InventoryHsnSac {
    id: string;
    name: string;
    hsnSac: number;
    sgst: number;
    cgst: number;
    igst: number;
    cess: number;
    codeType: GstCodeType;
}

export interface InventoryStore {
    id: string;
    name: string;
    address: string;
}
