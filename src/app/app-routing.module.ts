import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  LoginComponent,
  HomeComponent,
  InventoryHsnSacComponent,
  InventoryItemComponent,
  InventoryStoreComponent,
  InventoryUnitComponent,
  PurchaseComponent,
  PurchaseOrderComponent,
  PurchaseReturnComponent,
  ReportPriceListComponent,
  ReportPurchaseComponent,
  ReportPurchaseOrderComponent,
  ReportPurchaseReturnComponent,
  ReportSaleComponent,
  ReportSaleOrderComponent,
  ReportSaleReturnComponent,
  ReportStocksComponent,
  SaleComponent,
  SaleOrderComponent,
  SaleReturnComponent,
} from '../pages';
import { AuthGuard } from './../guards/auth.guard';

const routes: Routes = [{
  path: '',
  component: HomeComponent,
  canActivate: [AuthGuard],
}, {
  path: 'inventory-item',
  component: InventoryItemComponent,
  canActivate: [AuthGuard],
}, {
  path: 'inventory-hsn-sac',
  component: InventoryHsnSacComponent,
  canActivate: [AuthGuard],
}, {
  path: 'inventory-store',
  component: InventoryStoreComponent,
  canActivate: [AuthGuard],
}, {
  path: 'inventory-unit',
  component: InventoryUnitComponent,
  canActivate: [AuthGuard],
}, {
  path: 'login',
  component: LoginComponent
}, {
  path: '**',
  redirectTo: ''
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
