import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxElectronModule } from 'ngx-electron';

import {
  DropdownModule,
  InputTextModule,
  ButtonModule,
  DialogModule,
  MenubarModule,
  MessagesModule,
  PanelModule,
  FieldsetModule,
  ToolbarModule,
  MultiSelectModule,
  SliderModule,
  InputTextareaModule,
} from 'primeng/primeng';

import { MessageService } from 'primeng/api';

import { TableModule } from 'primeng/table'
import { ToastModule} from 'primeng/toast';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {
  ThemeSwitchComponent,
  TitleBarComponent,
} from './../components';

import {
  LoginComponent,
  HomeComponent,
  InventoryHsnSacComponent,
  InventoryItemComponent,
  InventoryStoreComponent,
  InventoryUnitComponent,
  PurchaseComponent,
  PurchaseOrderComponent,
  PurchaseReturnComponent,
  ReportPriceListComponent,
  ReportPurchaseComponent,
  ReportPurchaseOrderComponent,
  ReportPurchaseReturnComponent,
  ReportSaleComponent,
  ReportSaleOrderComponent,
  ReportSaleReturnComponent,
  ReportStocksComponent,
  SaleComponent,
  SaleOrderComponent,
  SaleReturnComponent,
} from '../pages';

import {
  AuthService,
} from './../services';

const SERVICES = [
  AuthService,
];

const PAGES = [
  LoginComponent,
  HomeComponent,
  InventoryHsnSacComponent,
  InventoryItemComponent,
  InventoryStoreComponent,
  InventoryUnitComponent,
  PurchaseComponent,
  PurchaseOrderComponent,
  PurchaseReturnComponent,
  ReportPriceListComponent,
  ReportPurchaseComponent,
  ReportPurchaseOrderComponent,
  ReportPurchaseReturnComponent,
  ReportSaleComponent,
  ReportSaleOrderComponent,
  ReportSaleReturnComponent,
  ReportStocksComponent,
  SaleComponent,
  SaleOrderComponent,
  SaleReturnComponent,
];

const PRIMENG_MODULES = [
  InputTextModule,
  ButtonModule,
  TableModule,
  DialogModule,
  MenubarModule,
  DropdownModule,
  ToastModule,
  MessagesModule,
  PanelModule,
  FieldsetModule,
  ToolbarModule,
  MultiSelectModule,
  SliderModule,
  InputTextareaModule,
];

const PRIMENG_SERVICES = [
  MessageService,
];

const COMPONENTS = [
  ThemeSwitchComponent,
  TitleBarComponent,
];

@NgModule({
  declarations: [
    AppComponent,
    ...COMPONENTS,
    ...PAGES,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    // ThemeModule,
    ...PRIMENG_MODULES,
    NgxElectronModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    ...PRIMENG_SERVICES,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
