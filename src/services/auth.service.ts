import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser = false;
  constructor(private router: Router) { }

  logout() {
    this.currentUser = false;
    this.router.navigate(['/login']);
  }
}
