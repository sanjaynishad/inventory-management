var gulp = require('gulp');
var run = require('gulp-run');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
var jeditor = require("gulp-json-editor");

function cleanDir() {
    return gulp.src('./gulp-build', { read: false })
        .pipe(clean());

}

function ngBuild() {
    return run('npm run build:prod').exec();
}

function copyAngularDist() {
    return gulp.src('./dist/**/*.*')
        .pipe(gulp.dest('./gulp-build/dist'))
}

function modifyPackage() {
    return gulp.src('./package.json')
        .pipe(jeditor(function (json) {
            delete json.dependencies;
            delete json.scripts;
            return json;
        }, {
                beautify: false
            }))
        .pipe(gulp.dest('./gulp-build'))
}

function copyFiles() {
    return gulp.src([
        './resources/**/*.*',
    ], {
            base: './'
        }).pipe(gulp.dest('gulp-build'));
}

function uglifyMainJs() {
    return gulp.src('./main.js')
        .pipe(uglify())
        .pipe(gulp.dest('./gulp-build'))
}

function electronBuild() {
    return run('npm run build:app').exec();
}

exports.cleanDir = cleanDir;
exports.ngBuild = ngBuild;
exports.copyAngularDist = copyAngularDist;
exports.uglifyMainJs = uglifyMainJs;
exports.copyFiles = copyFiles;
exports.electronBuild = electronBuild;
exports.modifyPackage = modifyPackage;

exports.default = gulp.series(cleanDir, ngBuild, copyAngularDist, modifyPackage, copyFiles, uglifyMainJs, electronBuild);
